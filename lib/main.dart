import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app_task/page/home_page_todo_app.dart';
import 'package:todo_app_task/provider/todosProvider.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'Todo App';

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => TodosProvider(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: title,
          theme: ThemeData(
            primarySwatch: Colors.red,
            scaffoldBackgroundColor: Color(0xFFf6f5ee),
            // scaffoldBackgroundColor: Color(0xFFB00020),
          ),
          home: HomePage(),
        ),
      );
}
