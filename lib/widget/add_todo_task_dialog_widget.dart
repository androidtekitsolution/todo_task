import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app_task/model/todo_model.dart';
import 'package:todo_app_task/provider/todosProvider.dart';
import 'package:todo_app_task/widget/todo_form_field_widget.dart';

class AddTodoTaskDialogWidget extends StatefulWidget {
  @override
  _AddTodoTaskDialogWidgetState createState() => _AddTodoTaskDialogWidgetState();
}

class _AddTodoTaskDialogWidgetState extends State<AddTodoTaskDialogWidget> {
  final _formKey = GlobalKey<FormState>();
  String title = '';
  String description = '';

  @override
  Widget build(BuildContext context) => AlertDialog(
        content: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Add Todo',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                    color: Colors.red,
                ),
              ),
              const SizedBox(height: 8),
              TodoFormFieldWidget(
                onChangedTitle: (title) => setState(() => this.title = title),
                onChangedDescription: (description) => setState(() => this.description = description),
                onSavedTodo: addTodo,
              ),
            ],
          ),
        ),
      );

  void addTodo() {
    final isValid = _formKey.currentState.validate();

    if (!isValid) {
      return;
    } else {
      final todo = Todo(
        id: DateTime.now().toString(),
        title: title,
        description: description,
        createdTime: DateTime.now(),
      );

      final provider = Provider.of<TodosProvider>(context, listen: false);
      provider.addTodo(todo);

      Navigator.of(context).pop();
    }
  }
}
