import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app_task/model/todo_model.dart';
import 'package:todo_app_task/provider/todosProvider.dart';
import 'package:todo_app_task/widget/todo_form_field_widget.dart';

class EditTodoListPage extends StatefulWidget {
  final Todo todo;

  const EditTodoListPage({Key key, @required this.todo}) : super(key: key);

  @override
  _EditTodoListPageState createState() => _EditTodoListPageState();
}

class _EditTodoListPageState extends State<EditTodoListPage> {
  final _formKey = GlobalKey<FormState>();

  String title;
  String description;

  @override
  void initState() {
    super.initState();

    title = widget.todo.title;
    description = widget.todo.description;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Edit Todo'),
          actions: [
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                final provider =
                    Provider.of<TodosProvider>(context, listen: false);
                provider.removeTodo(widget.todo);

                Navigator.of(context).pop();
              },
            )
          ],
        ),
        body: Padding(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: TodoFormFieldWidget(
              title: title,
              description: description,
              onChangedTitle: (title) => setState(() => this.title = title),
              onChangedDescription: (description) =>
                  setState(() => this.description = description),
              onSavedTodo: saveTodo,
            ),
          ),
        ),
      );

  void saveTodo() {
    final isValid = _formKey.currentState.validate();

    if (!isValid) {
      return;
    } else {
      final provider = Provider.of<TodosProvider>(context, listen: false);

      provider.updateTodo(widget.todo, title, description);

      Navigator.of(context).pop();
    }
  }
}
