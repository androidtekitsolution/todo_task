import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app_task/api/firebase_todo_list_api.dart';
import 'package:todo_app_task/model/todo_model.dart';
import 'package:todo_app_task/provider/todosProvider.dart';
import 'package:todo_app_task/widget/add_todo_task_dialog_widget.dart';
import 'package:todo_app_task/widget/completed_todo_list_widget.dart';
import 'package:todo_app_task/widget/todo_list_widget.dart';

import '../main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final tabs = [
      TodoListWidget(),
      CompletedTodoListWidget(),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text(MyApp.title),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white.withOpacity(0.7),
        selectedItemColor: Colors.white,
        currentIndex: selectedIndex,
        onTap: (index) => setState(() {
          selectedIndex = index;
        }),
        items: [
          // fact_check_outlined
          BottomNavigationBarItem(
            icon: Icon(Icons.list_alt),
            label: 'Todos List',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.download_done_outlined, size: 28),
            label: 'Completed Task',
          ),
        ],
      ),
      body: StreamBuilder<List<Todo>>(
        stream: FirebaseTodoListApi.readTodos(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            default:
              if (snapshot.hasError) {
                return buildText('Something Went Wrong Try later');
              } else {
                final todos = snapshot.data;

                final provider = Provider.of<TodosProvider>(context);
                provider.setTodos(todos);

                return tabs[selectedIndex];
              }
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        backgroundColor: Colors.red,
        onPressed: () => showDialog(
          context: context,
          builder: (context) => AddTodoTaskDialogWidget(),
          barrierDismissible: false,
        ),
        child: Icon(Icons.add),
      ),
    );
  }
}

Widget buildText(String text) => Center(
      child: Text(
        text,
        style: TextStyle(fontSize: 24, color: Colors.white),
      ),
    );
